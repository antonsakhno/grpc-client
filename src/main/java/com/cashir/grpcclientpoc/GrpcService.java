package com.cashir.grpcclientpoc;

import com.cashir.webfluxgrpcpoc.proto.PoCServiceGrpc.PoCServiceBlockingStub;
import com.cashir.webfluxgrpcpoc.proto.TestRequest;
import com.cashir.webfluxgrpcpoc.proto.TestResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class GrpcService {
    private PoCServiceBlockingStub serviceBlockingStub;

    public TestResponse doCallToGrpcService(String parameter) {
        return serviceBlockingStub.test(TestRequest.newBuilder()
                                                .setPropertyName1(parameter)
                                                .setPropertyName2("test 2")
                                                .build());
    }
}
