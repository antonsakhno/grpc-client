package com.cashir.grpcclientpoc;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.grpc.StatusRuntimeException;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@AllArgsConstructor
public class ClientController {
    private GrpcService grpcService;

    @GetMapping
    @ResponseBody
    public String testGrpc(@RequestParam Optional<String> parameter){
        return grpcService.doCallToGrpcService(parameter.orElse("null")).toString();
    }

    @ExceptionHandler(StatusRuntimeException.class)
    public String handleError(StatusRuntimeException e) {
        return e.toString();
    }

}
