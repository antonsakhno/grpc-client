package com.cashir.grpcclientpoc;

import com.cashir.webfluxgrpcpoc.proto.PoCServiceGrpc;
import com.cashir.webfluxgrpcpoc.proto.PoCServiceGrpc.PoCServiceBlockingStub;
import com.cashir.webfluxgrpcpoc.proto.PoCServiceGrpc.PoCServiceFutureStub;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    private ManagedChannel managedChannel() {
        return ManagedChannelBuilder.forAddress("localhost", 9090)
                .usePlaintext()
                .build();
    }

    @Bean
    public PoCServiceFutureStub serviceFutureStub() {
        return PoCServiceGrpc.newFutureStub(managedChannel());
    }

    @Bean
    public PoCServiceBlockingStub serviceBlockingStub() {
        return PoCServiceGrpc.newBlockingStub(managedChannel());
    }
}
